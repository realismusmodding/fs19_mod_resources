# FS19 mod resources

This repository contains some resources for modders that try to create
GEO mods or try to integrate their mods or maps with Seasons.


### GEO mod icons

There are two ways to create a GEO mod icon: using the template and using the background.

The template is the only official way to do it as it will provide correct fonts and positioning. If you are unable to use the template, send a message to Rahkiin from Realismus Modding and he will create an icon for you. The background is available in any case.

The template requires the 'Source Sans Pro' font to be installed (free font).

Try to use a vector image of your flag. You can find most by going to the Wikipedia page of the flag, going to the media resource, clicking on the link to the SVG original. There you can press Ctrl+S to save. After that you can drag and drop the file into Photoshop.
